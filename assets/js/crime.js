var base = '';

$(document).ready(function(){

	base = $("#base_url").val();

		$("#add_crime").click(function(){

			$("#myModal").modal('toggle');
		});

/*
	    $('[data-toggle="popover"]').popover({
	    	html : true,
	        placement : 'bottom'
	    });

*/

		$("#admin-menu").popover(
		    {
		        title: getPopTitle(),
		        content: getPopContent(),
		        placement : 'bottom',
		        html:true
		    }
		);

		function getPopTitle(){
			return "System Settings";
		}

		function getPopContent(){
			var str = "<div class='settings' role='menu'>";
				str +=  "<a href='" + base + "/systemadmin/addUser'><li>Add User</li> </a>";
				str += "<a href='" + base + "/systemadmin'><li>System Users</li></a>";
				str += "<a href='" + base + "/systemadmin/categories'><li>System Categories</li></a>";
				str += "<a href='" + base + "/systemadmin/changePassword'><li>Change Password</li></a>";
				str += "<a href='" + base + "/auth/logout'><li>Logout</li></a>";
			str += "</div>";

			return str;
		}


	     $('#cities-admin').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            numberDisplayed: 5,
            maxHeight : 200
        });

	     $('input.typeahead').typeahead({
       		  name: 'accounts',
			 // local: ['Audi', 'BMW', 'Bugatti', 'Ferrari', 'Ford', 'Lamborghini', 'Mercedes Benz', 'Porsche', 'Rolls-Royce', 'Volkswagen', 'Putol Ibabao, Cuenca,Batangas']
       		  prefetch: '<?=base()."/crimes/json"?>',
              limit: 10
    	 });

	     $("#province").change(function(){

	     		var province_id = $(this).val();

	     		$.ajax({

					type:       'POST',
					url:        base + "/systemadmin/getCities", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "province_id=" + province_id,
					dataType: 'json',
					success : function(result){

						$('#cities-admin').find('option').remove();

						$.each(result, function(i, item) {
							
    						$('#cities-admin').append('<option val="' + result[i].id + '">' + result[i].name + '</option>');
					
    						$('#cities-admin').multiselect('rebuild') 
						});

					}

				});


	     });

	
	
});


Global = {

	alert : function(title,content,callback){
		$('.modal-title').html(title);
		$('.modal-body').html(content);
		$('#h-yes').attr('onclick',callback);
		$("#global-alert").modal('toggle');

	},

	disableCategory : function(id,val) {
		
		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/disableCategory", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id +"&val=" + val,
					success : function(result){
						if (result == 'success'){
							window.location.reload();
						}
					}

		});

	},

	deleteCategory : function(id) {
		
		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/deleteCategory", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id,
					success : function(result){
						if (result == 'success'){
							window.location.reload();
						}
					}

		});

	},

	disableSubCategory : function(id,val) {
		
		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/disableSubCategory", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id +"&val=" + val,
					success : function(result){
						if (result == 'success'){
							window.location.reload();
						}
					}

		});

	},

	deleteSubCategory : function(id) {
		
		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/deleteSubCategory", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id,
					success : function(result){
						if (result == 'success'){
							window.location.reload();
						}
					}

		});

	},




	deactivateUser : function(id,val) {
		
		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/deactivateUser", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id + "&val=" + val,
					success : function(result){
						if (result == 'success'){
							window.location.reload();
						}
					}

		});

	},
	deleteUser : function(id) {
		
		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/deleteUser", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id,
					success : function(result){
						if (result == 'success'){
							window.location.reload();
						}
					}

		});

	},

	checkIfEmpty : function(frm){

		    var empty = false;
		    $('#' + frm +' .required').each(function(){
		    	
		        if($(this).val().trim()==""){
		            empty = true;
		            if (frm == 'frm_adduser')
		            	$(this).parent().parent().addClass("has-error");
		            else
		            	$(this).parent().addClass("has-error");
		        }else{
		        	 if (frm == 'frm_adduser')
		        	 	$(this).parent().parent().removeClass("has-error");
		        	 else
		           		$(this).parent().removeClass("has-error");
		        }


		    });

		    if (empty){
		
		    	$(".alert-danger").removeClass("hide");

		    }
    
    	return empty;
},


	updateCategory : function(id){

		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/getCategory", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id,
					dataType : 'json',
					success : function(result){

						$("#category_upd").val(result.category);
						$("#description_upd").val(result.description);
						$("#upd_category_id").val(id);
						$("#modal-category_upd").modal('toggle');

	
					}

		});
	},

	updateSubCategory : function(id){

		$.ajax({
					type:       'POST',
					url:        base + "/systemadmin/getSubCategory", 
					contentType: "application/x-www-form-urlencoded; charset=utf-8",
					data : "id=" + id,
					dataType : 'json',
					success : function(result){

						$("#subcategory_upd").val(result.sub_category);
						$("#subdescription_upd").val(result.sub_description);
						$("#upd_subcategory_id").val(id);
						$("#subcategory_id").val(result.category_id);
						$("#modal-subcategory_upd").modal('toggle');

	
					}

		});
	}



}


