<script type="text/javascript" src="<?=base()."/assets/js/highcharts.js"?>"></script>

<script type="text/javascript">
	$(function () {

    // Make monochrome colors and set them as default for all pies
    Highcharts.getOptions().plotOptions.pie.colors = (function () {
        var colors = [],
            base = Highcharts.getOptions().colors[0],
            i;

        for (i = 0; i < 10; i += 1) {
            // Start out with a darkened base color (negative brighten), and end
            // up with a much brighter color
            colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        }
        return colors;
    }());

    // Build the chart
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        credits: {
      		enabled: false
  		},
        title: {
            text: 'Crime Records 2015'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Crime Records',
            data: [
                ['Homicide',   45.0],
                ['Libel',       26.8],
                {
                    name: 'Theft',
                    y: 12.8,
                    sliced: true,
                    selected: true
                },
                ['Kidnapping',    8.5],
                ['Slander',     6.2],
                ['Threats',   0.7]
            ]
        }]
    });
});

</script>

<div class="page-header">
  <h3>Dashboard > <small>Overview</small></h2>
</div>

<div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">Summary of Crimes</div>
          <div class="panel-body">
           <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Larry</td>
          <td>the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </table>

          </div>
        </div>
</div>

<div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading">Chart Representation</div>
          <div class="panel-body">
            <div id="container" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto">

        </div>
          </div>
        </div>
</div>


