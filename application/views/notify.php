
<?php if ($this->session->flashdata('error')): ?>
	<div class="alert-notify" id="alert-notify">
		<div class="alert alert-danger">
			<button type="button" class="close"  onclick="$(this).closest('.alert-notify').fadeOut();">&times;</button>
			<p class="msg">
				<?php if (strpos($this->session->flashdata('error'),'<b>') === FALSE): ?>
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					<b>Error!</b>
				<?php endif ?>
				<?=$this->session->flashdata('error');?>
			</p>
		</div>
	</div>

<?php elseif ($this->session->flashdata('success')): ?>
	<div class="alert-notify" id="alert-notify">
		<div class="alert alert-success">
			<button type="button" class="close"  onclick="$(this).closest('.alert-notify').fadeOut();">&times;</button>
			<p class="msg">
				<?php if (strpos($this->session->flashdata('success'),'<b>') === FALSE): ?>
					<b>Well Done!</b> 
				<?php endif ?>
				<?=$this->session->flashdata('success');?>
			</p>
		</div>
	</div>
<?php elseif ($this->session->flashdata('notify')): ?>
	<div class="alert-notify" id="alert-notify">
		<div class="alert alert-info">
			<button type="button" class="close"  onclick="$(this).closest('.alert-notify').fadeOut();">&times;</button>
			<p class="msg">
				<?=$this->session->flashdata('notify');?>
			</p>
		</div>
	</div>
<?php endif ?>


<div class="alert alert-danger hide" role="alert">
	<button type="button" class="close"  onclick="$(this).closest('.alert-notify').fadeOut();">&times;</button>
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
 	Please check the empty fields.
</div>

