 <div class="page-header">
  <h3>System > <small>Change password</small> <span class=" glyphicon glyphicon-remove pull-right close" aria-hidden="true" onclick='window.location.href="<?=$_SERVER['HTTP_REFERER']?>"'></span> </h2>
</div>
<?php $this->load->view('notify'); ?>
 <div class="well">

<fieldset>
	<legend>Change password </legend>
	<form class="form-horizontal" method="POST" name="frm_changepassword" id="frm_changepassword" action="<?=base()."/systemadmin/updatePassword"?>" >
				 <div class="form-group">
				  	<div class="control-label col-sm-5 col-xs-12 ">
				 		 <i class="start" >*</i> Old password
				 	 </div>
					  <div class="col-sm-4 col-xs-5"> 
					  	 <input name="old" type="text" class="form-control required" id="inputEmail3" value="" >
					  </div>

				  </div>

				   <div class="form-group">
				  	<div class="control-label col-sm-5 col-xs-12 ">
				 		 <i class="start" >*</i> New password
				 	 </div>
					  <div class="col-sm-4 col-xs-5"> 
					  	 <input name="new" type="text" class="form-control required" id="inputEmail3" value="" >
					  </div>

				  </div>

				   <div class="form-group">
				  	<div class="control-label col-sm-5 col-xs-12 ">
				 		 <i class="start" >*</i> Repeat Password
				 	 </div>
					  <div class="col-sm-4 col-xs-5"> 
					  	 <input name="repeat" type="text" class="form-control required" id="inputEmail3" value="" >
					  </div>

				  </div>
				</form>
</fieldset>

</div>

<div class="form-btn-actions pull-right">
	<!-- Split button -->
	 <button type="button" class="btn btn-default" onclick = "window.location.href='<?=base()."/systemadmin"?>'">Cancel</button>

		<div class="btn-group">
		
		  <button type="submit" class="btn btn-success" onclick = "if(!Global.checkIfEmpty('frm_changepassword')) { $('#frm_changepassword').submit(); }" >Save</button>
		 
		</div>

</div>
