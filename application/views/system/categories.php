<div class="page-header">
  <h3>System > <small>Categories</small>  <span class=" glyphicon glyphicon-remove pull-right close" aria-hidden="true" onclick='window.location.href="<?=$_SERVER['HTTP_REFERER']?>"'></span> </h2>
		         
</div>
<?php $this->load->view('notify'); ?>
<div class="well">
<fieldset>
	<legend>Crime Categories</legend>
		<form id="frm-categories"class="form-inline" method="POST" action = "<?=base()."/systemadmin/insertCategory"?>">
		  <div class="form-group">
		    <label for="categoryname">Category name</label>
		    <input type="text" class="form-control required" id="category" placeholder="Category" name='category'>
		  </div>
		  <div class="form-group">
		    <label for="categorydesc">Description</label>
		    <input type="text" class="form-control required" id="description" placeholder="Description" name='description'>
		  </div>
		  <input type="button" class="btn btn-success" value="save" onclick="if(!Global.checkIfEmpty('frm-categories')){ $('#frm-categories').submit();}" />
		</form>
</fieldset>
</div>

<table class="table table-striped">
      <thead>
        <tr>
        
          <th>Ref. id</th>
          <th>Category</th>
          <th>Description</th>
          <th>Enable</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      	<?php foreach ($categories as $key => $category) { ?>
     
		        <tr>
		          <td scope="row"><?=$category['category_id']?></td>
		          <td><?=$category['category']?></td>
		          <td><?=$category['description']?></td>
		          <td>
		          		<?php if($category['enabled'] == 1) { ?>
		          			<span class="glyphicon glyphicon-ok link-blue" aria-hidden="true"></span>
		          		<?php }else { ?>
		          			<span class="glyphicon glyphicon-remove"  aria-hidden="true"></span>
		          		<?php } ?>
		          </td>

		          <td>

		          		 <div class="form-btn-actions pull-right">
                        <!-- Split button -->
                          <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-list" onclick = "Global.updateCategory(<?=$category['category_id']?>)" >Edit</button>
                            <button type="button" class="btn btn-primary btn-list dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                            	<?php if($category['enabled'] == 1) { ?>
                             		 <li><a href="javascript:void(0)" onclick="Global.alert('Disable Category','Are you sure you want to disable?','Global.disableCategory(<?=$category['category_id']?>,0)')">Disable</a></li>
                              <?php }else { ?>
                              		<li><a href="javascript:void(0)" onclick="Global.alert('Enable Category','Are you sure you want to enable?','Global.disableCategory(<?=$category['category_id']?>,1)')">Enable</a></li>
                              <?php } ?>
                              <li><a href="javascript:void(0)"  onclick="Global.alert('Delete Category','Are you sure you want to delete?','Global.deleteCategory(<?=$category['category_id']?>)')">Delete</a></li>
                            </ul>
                          </div>

                      </div>


		          </td>
		        </tr>

        	<?php } ?>
       
      </tbody>
    </table>



<div class="well">
<fieldset>
	<legend>Crime Sub Categories</legend>
	
		<form id="frm-sub-categories" class="form-inline" method = "post" action = "<?=base()."/systemadmin/insertSubCategory"?>">
		 
		  <div class="form-group">
		    <label for="exampleInputName2">Category name</label>

		    <input type="text" class="form-control required" id="sub_category" placeholder="Category" name="sub_category">
		  
		  </div>


		  <div class="form-group">
		    <label for="exampleInputEmail2">Category</label>
		    <select name="category_id"  id="sel-categories" class="form-control required">
		    		<?php foreach ($categories as $key => $category) { ?>
		    			<?php if ($category['enabled'] == 1) { ?>
		   				 	 <option value="<?=$category['category_id']?>" ><?=$category['category']?></option>
		   				  <?php } ?>
		   	    	<?php } ?>
		    </select>
		  </div>

		   <div class="form-group">
		    <label for="exampleInputEmail2">Description</label>
		    <input type="text" class="form-control required" id="sub_description" placeholder="Description" name="sub_description">
		  </div>

		  <button type="button" class="btn btn-success"  onclick="if(!Global.checkIfEmpty('frm-sub-categories')){ $('#frm-sub-categories').submit();}" >Save</button>
		</form>
</fieldset>
</div>


<table class="table table-striped">
      <thead>
        <tr>
	          <th>Ref. id</th>
	          <th>Sub Category</th>
	          <th>Category</th>
	          <th>Description</th>
	          <th>Enabled</th>
	          <th></th>
        </tr>
      </thead>
      <tbody>

      	<?php foreach ($sub_categories as $key => $category) { ?>
	        <tr>
	          <td scope="row"><?=$category['sub_category_id']?></td>
	          <td><?=$category['sub_category']?></td>
	          <td><?=$category['category']?></td>
	          <td><?=$category['sub_description']?></td>
	          <td>
	          			<?php if($category['enabled'] == 1) { ?>
		          			<span class="glyphicon glyphicon-ok link-blue" aria-hidden="true"></span>
		          		<?php }else { ?>
		          			<span class="glyphicon glyphicon-remove"  aria-hidden="true"></span>
		          		<?php } ?>

	          </td>
	          <td>

	          	 <div class="form-btn-actions pull-right">
                        <!-- Split button -->
                          <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-list" onclick = "Global.updateSubCategory(<?=$category['sub_category_id']?>)" >Edit</button>
                            <button type="button" class="btn btn-primary btn-list dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">

                              <?php if($category['enabled'] == 1) { ?>
                             		 <li><a href="javascript:void(0)" onclick="Global.alert('Disable Category','Are you sure you want to disable?','Global.disableSubCategory(<?=$category['sub_category_id']?>,0)')">Disable</a></li>
                              <?php }else { ?>
                              		<li><a href="javascript:void(0)" onclick="Global.alert('Enable Category','Are you sure you want to enable?','Global.disableSubCategory(<?=$category['sub_category_id']?>,1)')">Enable</a></li>
                              <?php } ?>

                              <li><a href="javascript:void(0)" onclick="Global.alert('Delete Category','Are you sure you want to delete?','Global.deleteSubCategory(<?=$category['sub_category_id']?>)')">Delete</a></li>
                            </ul>
                          </div>

                      </div>

	          </td>
	        </tr>
 		<?php } ?>
      </tbody>
    </table>



	<div id="modal-subcategory_upd" class="modal fade bs-example-modal-lg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Category</h4>
            </div>
            <div class="modal-body">

            	<form id="frm-subcategories-upd"class="form-horizontal" method="POST" action = "<?=base()."/systemadmin/updateSubCategory"?>">
					  <input type = "hidden" name= "upd_subcategory_id" value="" id = "upd_subcategory_id" />
					  
					  <div class="form-group ">
					    <label for="categoryname">Category name</label>
					    <input type="text" class="form-control required" id="subcategory_upd" placeholder="Category" name='subcategory_upd'>
					  </div>

					   <div class="form-group">
					    <label for="exampleInputEmail2">Category</label>
					    <select name="subcategory_id"  id="sel-categories" class="form-control required">
					    		<?php foreach ($categories as $key => $category) { ?>
					    			<?php if ($category['enabled'] == 1) { ?>
					   				 	 <option value="<?=$category['category_id']?>" ><?=$category['category']?></option>
					   				  <?php } ?>
					   	    	<?php } ?>
					    </select>
					  </div>


					  <div class="form-group col-md-12">
					    <label for="categorydesc">Description</label>
					    <input type="text" class="form-control required" id="subdescription_upd" placeholder="Description" name='subdescription_upd'>
					  </div>
					
				</form>

            </div>
            <div class="modal-footer">    
                <button type="button" class="btn btn-default"  data-dismiss="modal" >Cancel</button>
                <button type="button" class="btn btn-primary"  onclick="if(!Global.checkIfEmpty('frm-subcategories-upd')) { $('#frm-subcategories-upd').submit();}" >Update</button>
            </div>
        </div>
    </div>
</div>


<div id="modal-category_upd" class="modal fade bs-example-modal-lg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Category</h4>
            </div>
            <div class="modal-body">
            	
            	<form id="frm-categories-upd"class="form-horizontal" method="POST" action = "<?=base()."/systemadmin/updateCategory"?>">
					  <input type = "hidden" name= "upd_category_id" value="" id = "upd_category_id" />
					  
					  <div class="form-group col-md-12">
					    <label for="categoryname">Category name</label>
					    <input type="text" class="form-control required" id="category_upd" placeholder="Category" name='category_upd'>
					  </div>



					  <div class="form-group col-md-12">
					    <label for="categorydesc">Description</label>
					    <input type="text" class="form-control required" id="description_upd" placeholder="Description" name='description_upd'>
					  </div>
					
				</form>

            </div>
            <div class="modal-footer">    
                <button type="button" class="btn btn-default"  data-dismiss="modal" >Cancel</button>
                <button type="button" class="btn btn-primary"  onclick="if(!Global.checkIfEmpty('frm-categories-upd')) { $('#frm-categories-upd').submit();}" >Update</button>
            </div>
        </div>
    </div>
</div>

