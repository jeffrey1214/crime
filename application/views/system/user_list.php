<div class="page-header">
  <h3>System > <small>Users</small></h2>
</div>

<?php $this->load->view('notify'); ?>

<div class = "pull-right" >
  
  <button type="button" class="btn btn-danger" id="add_crime">Delete</button>
   <button type="button" class="btn btn-default" data-toggle="popover" title="Popover title" data-content="A larger popover to demonstrate the max-width of the Bootstrap popover.">Search</button>
  <button type="button" class="btn btn-default">Export</button>
  <a href="<?=base()."/systemadmin/addUser"?>" class="btn btn-primary btn-flat" >Add New</a>
</div>


<table class="table table-striped system-users">
      <thead>
        <tr>
          <th><input type="checkbox" name="checkall" id="checkAll" /></th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Username</th>
          <th>Address</th>
          <th>Contact</th>
           <th>Role</th>
          <th>Active</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>

      <?php foreach ($users as $user) { ?>
            <tr>
              <th scope="row"><input type="checkbox" name="user_id[]" value="<?=$user['user_id']?>" /></th>
              <td><?=$user['first_name']?></td>
              <td><?=$user['last_name']?></td>
              <td><?=$user['username']?></td>
               <td><?=$user['address']?></td>
              <td><?=$user['mobile']?></td>
              <td><?=($user['role_id'] == 1) ? 'Super admin' : 'Admin'?></td>
              <td>
                <?php if($user['active'] == 1) { ?>
                    <span class="glyphicon glyphicon-ok link-blue" aria-hidden="true"></span>
                  <?php }else { ?>
                    <span class="glyphicon glyphicon-remove"  aria-hidden="true"></span>
                  <?php } ?>

              </td>
              <td>

                      <div class="form-btn-actions pull-right">
                        <!-- Split button -->
                          <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-list" onclick = "window.location.href = '<?=base()."/systemadmin/addUser/".$user['user_id']?>'" >Open</button>
                            <button type="button" class="btn btn-primary btn-list dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <?php if($user['active'] == 1) { ?>
                                  <li><a href="javascript:void(0)" onclick="Global.alert('Deactivate User','Are you sure you want to deactivate?','Global.deactivateUser(<?=$user['user_id']?>,0)')">Deactivate</a></li>
                               <?php }else { ?>
                                   <li><a href="javascript:void(0)" onclick="Global.alert('Activate User','Are you sure you want to activate?','Global.deactivateUser(<?=$user['user_id']?>,1)')">Activate</a></li>
                                 <?php } ?>
                              <li><a href="javascript:void(0)">Reset Password</a></li>
                              <li><a href="javascript:void(0)" onclick="Global.alert('Delete User','Are you sure you want to delete?','Global.deleteUser(<?=$user['user_id']?>)')">Delete</a></li>
                            </ul>
                          </div>

                      </div>

              </td>


            </tr>
        <?php } ?>
       
      </tbody>
    </table>

 
    <div class="pagination pagination-centered">
              <ul class="pagination">
                <?=$links?>
             </ul>
            </div>
