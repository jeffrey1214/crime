<div class="page-header">
  <h3>System > <small>Add User</small> <span class=" glyphicon glyphicon-remove pull-right close" aria-hidden="true" onclick='window.location.href="<?=$_SERVER['HTTP_REFERER']?>"'></span> </h2>
</div>

<?php $this->load->view('notify'); ?>

	<form class="form-horizontal" method="POST" name="frm_adduser" id="frm_adduser" action="<?=base()."/systemadmin/insertUser"?>" >
			<input type="hidden" name="user_id" value="<?=$user['user_id']?>" />
			<div class="well" >

			<fieldset>
				<legend>Information</legend>

				<div class="form-group">
				  	<div class="control-label col-sm-5 col-xs-12 ">
				 		 Active user
				 	 </div>
					  <div class="col-sm-4 col-xs-5"> 
					  	 <label for="active_yes" class="radio-inline">
							<input type="radio" id="active_yes" name="active" value="1" <?=($user['active'] == 1 ? "checked" : "")?>> Yes 
						</label>
						<label for="active_no" class="radio-inline">
							<input type="radio" id="active_no" name="active" value="0" <?=($user['active'] == 0 ? "checked" : "")?>> No 
						</label>

					  </div>

				  </div>


				  <div class="form-group">
				  	<div class="control-label col-sm-5 col-xs-12 ">
				 		 <i class="start" >*</i> First Name
				 	 </div>
					  <div class="col-sm-4 col-xs-5"> 
					  	 <input name="first_name" type="text" class="form-control required" id="inputEmail3" value="<?=$user['first_name']?>" >
					  </div>

				  </div>

				   <div class="form-group">
				  	<div class="control-label col-sm-5 col-xs-12">
				 		 <i class="start" >*</i> Last Name
				 	 </div>
				  <div class="col-sm-4 col-xs-5"> 
				  	 <input name="last_name" type="text" class="form-control required" id="inputEmail3" value="<?=$user['last_name']?>" >
				  </div>
				  </div>

				   <div class="form-group">
				  	<div class="control-label col-sm-5 col-xs-12">
				 		 Middle Name
				 	 </div>
				  <div class="col-sm-4 col-xs-5"> 
				  	 <input name="middle_name" type="text" class="form-control" id="inputEmail3" value="<?=$user['middle_name']?>" >
				  </div>
				  </div>

				    <div class="form-group">
				  		<div class="control-label col-sm-5 col-xs-12">
				 		 <i class="start" >*</i> Contact No
				 	 	</div>
				 	 <div class="col-sm-4 col-xs-5"> 
				  	 	<input name="contact" type="text" class="form-control required" id="inputEmail3" value="<?=$user['mobile']?>" >
				  	</div>
				  </div>

				    <div class="form-group">
				  		<div class="control-label col-sm-5 col-xs-12">
				 		 <i class="start" >*</i> Email
				 	 	</div>
				 	 <div class="col-sm-4 col-xs-5"> 
				  	 	<input name="email" type="email" class="form-control required" id="inputEmail3" placeholder="i.e sample@sample.com" value="<?=$user['email']?>" >
				  	</div>
				  </div>
				 	  <div class="form-group">
				  		<div class="control-label col-sm-5 col-xs-12">
				 		Address
				 	 	</div>
				 	 <div class="col-sm-4 col-xs-5"> 
				  	 	<textarea name='address' class="form-control" id="inputEmail3" placeholder="email" > <?=$user['address']?></textarea>
				  	</div>
				  </div>

				  	<div class="form-group">
				  		<div class="control-label col-sm-5 col-xs-12">
				 		Role
				 	 	</div>
				 	 <div class="col-sm-4 col-xs-5"> 
				  	 	 <select id = "role" name="role" class="form-control selectpicker" >
				      		<option value = "1" <?=($user['role_id'] == 1) ? "selected" : ""?> >Super Admin </option>
				      		<option value = "2" <?=($user['role_id'] == 2) ? "selected" : ""?> >Admin </option>
				      	</select>
				  	</div>
				  </div>

				 </div>

			</fieldset>

				<div class="well" >

				<fieldset>
					<legend>Assign Location</legend>

					<div class="form-group">
				  		<div class="control-label col-sm-5 col-xs-12">
				 		 <i class="start" >*</i> Province
				 	 	</div>
				 	 <div class="col-sm-4 col-xs-5"> 
				  	 	 <select id="province" name="province_id" class="form-control selectpicker required">
				  	 	 <option value="0">--Select--</option>
				      		<?php foreach($provinces as $province){ ?>
				      			<option value="<?=$province['id']?>" <?=($user['province_id'] == $province['id']) ? "selected" : ""?> > <?=$province['name']?></option>
				      		<?php } ?>
				      	</select>
				  	</div>
				  </div>


				  <div class="form-group">
				  		<div class="control-label col-sm-5 col-xs-12">
				 		 <i class="start" >*</i> City
				 	 	</div>
				 	 <div class="col-sm-4 col-xs-5"> 
				 	 	<?php $arr = array(); ?>
				  	 	  <select id="cities-admin" name="cities[]" multiple="multiple" class="">
				  	 	  	<?php foreach($location as $city) { ?>
				  	 	  		<?php $arr[] = $city['id']; ?>
				  	 	  		<option value="<?=$city['id']?>" selected><?=$city['name']?></option>
				  	 	  	<?php } ?>

				  	 	  	<?php foreach($cities as $city) { ?>
				  	 	  		<?php if(!in_array($city['id'], $arr)) { ?>
				  	 	  			<option value="<?=$city['id']?>" ><?=$city['name']?></option>
				  	 	  		<?php } ?>
				  	 	  	<?php } ?>

			              </select>
				  	</div>
				  </div>

				</div>

				</fieldset>


</form>


<div class="form-btn-actions pull-right">
	<!-- Split button -->
	 <button type="button" class="btn btn-default" onclick = "window.location.href='<?=base()."/systemadmin"?>'">Cancel</button>

		<div class="btn-group">
		  <button type="submit" class="btn btn-success" onclick = "if(!Global.checkIfEmpty('frm_adduser')) { $('#frm_adduser').submit(); }" >Save</button>
		  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		    <span class="caret"></span>
		    <span class="sr-only">Toggle Dropdown</span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
		    <li><a href="javascript:void(0)">Save & New</a></li>
		  </ul>
		</div>

</div>




<br>
<br>
<br>