<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Crime Mapping System</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="<?=base()."/assets/css/bootstrap.min.css"?>" />
		<!-- Optional theme -->
		<link rel="stylesheet" href="<?=base()."/assets/css/bootstrap-theme.min.css"?>" />
		<link rel="stylesheet" href="<?=base()."/assets/css/bootstrap-multiselect.css"?>" />
		<link rel="stylesheet" href="<?=base()."/assets/css/style.css"?>" />

		<link rel="stylesheet" href="<?=base()."/assets/css/select2.min.css"?>" />

		<!-- Latest compiled and minified JavaScript -->
		
		<script type="text/javascript" src="<?=base()."/assets/js/jquery-1.11.2.min.js"?>"></script>
		<script src="<?=base()."/assets/js/bootstrap.min.js"?>"></script>
		<script src="<?=base()."/assets/js/bootstrap-multiselect.js"?>"></script>
		<script src="<?=base()."/assets/js/bootstrap3-typeahead.min.js"?>"></script>
		<script src="<?=base()."/assets/js/select.min.js"?>"></script>
		<script src="<?=base()."/assets/js/select2.min.js"?>"></script>
		<script src="<?=base()."/assets/js/crime.js"?>"></script>
		<script type="text/javascript">
			
		</script>

	

	</head>
<body>

