<?php $this->load->view('header'); ?>

<div class="container">
	<br>
	
	<div class = "login-page">
			 <form class="form-signin" method="post" action="<?=base()."/auth/login"?>">
			        <h2 class="form-signin-heading">Please sign in</h2>

			        <?php if ($this->session->flashdata('error')){ ?>
			        	<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Error!</strong> <?=$this->session->flashdata('error')?> 
						</div>

			        <?php } ?>


			        <label for="inputEmail" class="sr-only">Email address</label>
			        <input name="username" type="email" id="inputEmail" class="form-control" placeholder="Email address"  autofocus>
			        <label for="inputPassword" class="sr-only">Password</label>
			        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" >
			        <div class="checkbox">
			          <label>
			            <input name="remember_me" type="checkbox" value="remember-me"> Remember me
			          </label>
			        </div>
			        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign in" />
     		 </form>
	</div>

</div>

<footer class="footer">
      <div class="container" style ="margin-top:10px">
        <p class="text-muted text-center">Copyright 2015</p>
      </div>
    </footer>

</body>
</html>
