<?php


if ( ! function_exists('base'))
{	
	function base()
	{
		return "http://".$_SERVER['SERVER_NAME'];
	}
}


if ( ! function_exists('pre'))
{	
	function pre($array)
	{
		echo "<pre>",print_r($array),"</pre>";
	}
}


if ( ! function_exists('getRandomString'))
{	
	
	function getRandomString($length = 6) {
		
	    $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ123456789";
	    $validCharNumber = strlen($validCharacters);
	 
	    $result = "";
	 
	    for ($i = 0; $i < $length; $i++) {
	        $index = mt_rand(0, $validCharNumber - 1);
	        $result .= $validCharacters[$index];
	    }
	 
	    return $result;
	}

}


if ( ! function_exists('esc'))
{
	function esc($str){
		$CI =& get_instance();

		return $CI->db->escape($str);
	}
}
