<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_model extends CI_Model {

	public function getProvinces(){
		return $this->db->query("SELECT * from provinces order by name")->result_array();
	}

	public function getCities($province_id){
		return $this->db->query("SELECT * from cities WHERE province_id = $province_id order by name")->result_array();
	}

	public function insertUser($data){
		$sql = "INSERT INTO system_users(username,password,first_name,last_name,middle_name,address,mobile,email,role_id,province_id,active,created_by,created_date,tmp_password)
			VALUES('$data[username]',MD5('$data[password]'),'$data[first_name]','$data[last_name]','$data[middle_name]','$data[address]','$data[contact]','$data[email]',$data[role],$data[province_id],$data[active],$data[created_by],CURRENT_TIMESTAMP,1)";
		$this->db->query($sql);
		return $this->db->insert_id();

	}

	public function updateUser($data){
		$sql = "UPDATE system_users SET first_name = ".esc($data['first_name']).",
			last_name=".esc($data['last_name']).",
			middle_name=".esc($data['middle_name']).",
			address = ".esc($data['address']).",
			mobile= ".esc($data['contact']).",
			email=".esc($data['email']).",
			role_id=".$data['role'].",
			province_id=".$data['province_id'].",
			active=".$data['active']." where user_id=".$data['user_id'];

		$this->db->query($sql);

	}



	public function insertLocation($user_id,$city_id){
		$sql = "INSERT INTO user_location(user_id,city_id) VALUES($user_id,$city_id)";
		$this->db->query($sql);
	}

	public function deleteLocation($user_id){
		$this->db->query("DELETE FROM user_location where user_id = $user_id");

	}

	public function getUsers(){
		return $this->db->query("SELECT * from system_users")->result_array();
	}

	public function getLocation($id){
		return $this->db->query("select * from user_location ul inner join cities c on c.id = ul.city_id where ul.user_id = $id")->result_array();
	}

	public function getUser($user_id){
		return $this->db->query("SELECT * from system_users WHERE user_id = $user_id")->row_array();
	}

	public function deactivate($user_id){
		$this->db->query("UPDATE system_users SET active = 0 WHERE user_id = $user_id");
	}

	public function insertCategory($category,$description){
		$this->db->query("INSERT INTO system_categories(category,description) 
					VALUES(".esc($category).",".esc($description).")");
	}

	public function insertSubCategory($category_id,$sub_category,$sub_description){
		$this->db->query("INSERT INTO system_sub_categories(category_id,sub_category,sub_description) 
					VALUES($category_id,".esc($sub_category).",".esc($sub_description).")");
	}

	public function getCategories(){
		return $this->db->query("SELECT * from system_categories")->result_array();
	}

	public function getSubCategories(){
		return $this->db->query("SELECT ssc.*,category from system_sub_categories ssc inner join system_categories sc on sc.category_id = ssc.category_id")->result_array();
	}

	public function disableCategory($id,$val){
		$this->db->query("update system_categories set enabled = $val where category_id = $id");
	}

	public function deleteCategory($id){
		$this->db->query("DELETE from system_categories where category_id = $id");
	}

	public function disableSubCategory($id,$val){
		$this->db->query("update system_sub_categories set enabled = $val where sub_category_id = $id");
	}

	public function deleteSubCategory($id){
		$this->db->query("DELETE from system_sub_categories where sub_category_id = $id");
	}

	public function deactivateUser($user_id,$val){
		$this->db->query("UPDATE system_users set active = $val where user_id = $user_id");
	}

	public function deleteUser($user_id){
		$this->db->query("DELETE FROM system_users where user_id = $user_id");
	}

	public function getCategory($id){
		return $this->db->query("SELECT * from system_categories where category_id = $id")->row_array();

	}

	public function getSubCategory($id){
		return $this->db->query("SELECT * from system_sub_categories where sub_category_id = $id")->row_array();

	}


	public function updateCategory($data){
		$this->db->query("update system_categories set category= ".esc($data['category_upd']).",
			description=".esc($data['description_upd'])." where category_id = ".$data['upd_category_id']);
		
	}

	public function updateSubCategory($data){
		$this->db->query("update system_sub_categories set sub_category= ".esc($data['subcategory_upd']).",
			sub_description=".esc($data['subdescription_upd']).",category_id = $data[subcategory_id] where sub_category_id = ".$data['upd_subcategory_id']);
		
	}

	public function updatePassword($user_id,$password){
		$this->db->query("update system_users set password = md5(".esc($password).") where user_id = $user_id");
	}


}
