<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Controller extends CI_Controller {

	public function __construct(){

		 parent::__construct();
 		 $this->load->library('session');
 		 $this->load->helper('url');
		 //check for session

		 if ($this->session->userdata('logged') == 1){
		 	//do nothing, the user has logged
		 }else{
		 	redirect(base()."/auth");
		 }
	}

}