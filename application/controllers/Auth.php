<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		 parent::__construct();
		 $this->load->model('auth_model');
		 $this->load->helper('url');
		 $this->load->helper('cookie');
	}


	public function index(){
			
		$this->load->view('login');
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base()."/auth");
	}

	public function login(){
		$user_dtls = $this->auth_model->getLoginDtls($this->input->post('username'),$this->input->post('password'));

		if (!empty($user_dtls)){

			$this->auth_model->updateLastLogin($user_dtls['user_id']);

			if (array_key_exists("remember_me", $_POST)){
				//set data to cookie
			}else{
				//delete data from cookie
			}

			$user_dtls['logged'] = true;
			$this->session->set_userdata($user_dtls);
			redirect(base()."/dashboard");

		}else{
			$this->session->set_flashdata('error','Invalid username/password!');
			redirect(base()."/auth");
		}
	}

}