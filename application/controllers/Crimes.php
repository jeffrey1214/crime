<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crimes extends MY_Controller {

	public function __construct(){
		 parent::__construct();
	}

	public function index(){
		$data['page'] = 'listings';
		$this->load->view('template',$data);
	}

	public function json(){
		$arr = array("Manila","Batangas","laguna");
		header('Content-Type: application/json');
		echo json_encode($arr);


	}

}
