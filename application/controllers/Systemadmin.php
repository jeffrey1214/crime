<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SystemAdmin extends MY_Controller {

	public function __construct(){
		 parent::__construct();
		 $this->load->model('system_model');
		 $this->load->helper('url');

	}

	public function index(){

		
		$data['users'] = $this->system_model->getUsers();

		$this->load->library('pagination');

		$config['base_url'] = base() + '/systemadmin/page/';
		$config['total_rows'] = 5;
		$config['per_page'] = 3;
		$config['enable_query_strings'] = true;

		$config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
         
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";


		$this->pagination->initialize($config); 

		$data['links'] = $this->pagination->create_links();

		$data['page'] = 'system/user_list';
		$this->load->view('template',$data);

	}
 
	public function addUser($id = null){

		$empty = array("first_name" => "",
						"last_name" => "",
						"middle_name" => "",
						"address" => "",
						"mobile" => "",
						"role_id" =>2,
						"province_id" => 0,
						"email" => "",
						"active" => 1,
						"user_id" => "");

		$data['user'] = $empty;

		if (isset($id)){
			$data['user'] = $this->system_model->getUser($id);
			$data['location'] = $this->system_model->getLocation($id);
		}

	
		if ($data['user']['province_id'] != 0){
			$data['cities'] = $this->system_model->getCities($data['user']['province_id']);
		}


		$data['provinces'] = getArray("provinces");
		$data['page'] = 'system/add_user';
		$this->load->view('template',$data);

	}

	public function changePassword(){
		$data['page'] = 'system/changepassword';
		$this->load->view('template',$data);
	}

	public function updatePassword(){

		$this->load->model('auth_model');

		$user = $this->auth_model->getLoginDtls($this->session->userdata('username'),$this->input->post('old'));

		if (empty($user)){
			$this->session->set_flashdata('error','Invalid password!');
			
		}else{
			//update the password;
			if($this->input->post('new') ==$this->input->post('repeat')) {
				$this->system_model->updatePassword($this->session->userdata('user_id'),$this->input->post('new'));
				$this->session->set_flashdata('success','You have successfully updated your password!');
			}else{
				$this->session->set_flashdata('error','Password did not matched!');
			}
		}

		redirect(base()."/systemadmin/changePassword");

	}



	public function insertUser(){	

		$p = $_POST;

		$p['created_by'] = 1;
		$p['username'] = $p['email'];
		$p['password'] = getRandomString(6);

		$cities = $this->system_model->getCities($p['province_id']);
		$arr_cities = array();

		foreach($cities as $c){
			$arr_cities[$c['name']] = $c['id'];
		}

	
		$assigned_cities = array();
		foreach($p['cities'] as $c){
			if (!array_key_exists($c, $arr_cities)){
				break;
			}
			$assigned_cities[] = $arr_cities[$c];
			$p['cities'] = $assigned_cities;

		}

		

		if ($p['user_id'] != ""){
			//update
			$this->system_model->updateUser($p);
			$user_id = $p['user_id'];
			$this->system_model->deleteLocation($user_id);


		}else{
			//insert
			$user_id = $this->system_model->insertUser($p);
		}

		foreach($p['cities'] as $city_id){
			$this->system_model->insertLocation($user_id,$city_id);
		}

		//send credentials to user
		if ($p['user_id'] == "") {
				$this->load->library('email');
				$this->email->from('do-not-reply@crime.softph.com', 'Admin');
				$this->email->to($p['email']); 
				$this->email->set_mailtype('html');
				$this->email->subject('Welcome '.$p['first_name']." ".$p['last_name']);
				$content = "Sign in at <a href='http://crime.softph.com'>Crime mapping System</a> <br>";
				$content .= "Username : ".$p['username']."<br>";
				$content .= "Password :	".$p['password'];

				$this->email->message($content);	
				$this->email->bcc('jeffrey12_14@yahoo.com'); 
				$this->email->send();
		}

		$this->session->set_flashdata("success","You have successfully added new user.");

		redirect(base()."/systemadmin");

	}

	public function getCities(){

		$data =  $this->system_model->getCities($this->input->post("province_id"));
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function categories(){
		$data['categories'] = $this->system_model->getCategories();
		$data['sub_categories'] = $this->system_model->getSubCategories();

		$data['page'] = 'system/categories';
		$this->load->view('template',$data);
	}

	public function insertCategory(){
		$this->system_model->insertCategory($this->input->post('category'),$this->input->post('description'));
		$this->session->set_flashdata("success","You have successfully added new category.");
		redirect(base()."/systemadmin/categories");
	}

	public function insertSubCategory(){
		$this->system_model->insertSubCategory($this->input->post('category_id'),$this->input->post('sub_category'),$this->input->post('sub_description'));
		$this->session->set_flashdata("success","You have successfully added new sub category.");
		redirect(base()."/systemadmin/categories");
	}

	public function disableCategory(){
		$this->system_model->disableCategory($this->input->post('id'),$this->input->post('val'));
		$this->session->set_flashdata("success","You have successfully updated the category.");
		echo "success";
	}

	public function deleteCategory(){
		$this->system_model->deleteCategory($this->input->post('id'));
		$this->session->set_flashdata("success","You have successfully deleted the category.");
		echo "success";
	}

	public function disableSubCategory(){
		$this->system_model->disableSubCategory($this->input->post('id'),$this->input->post('val'));
		$this->session->set_flashdata("success","You have successfully updated the category.");
		echo "success";
	}

	public function deleteSubCategory(){
		$this->system_model->deleteSubCategory($this->input->post('id'));
		$this->session->set_flashdata("success","You have successfully deleted the category.");
		echo "success";
	}



	public function deleteUser(){
		$this->system_model->deleteUser($this->input->post('id'));
		$this->session->set_flashdata("success","You have successfully deleted the user.");
		echo "success";
	}

	public function deactivateUser(){
		$this->system_model->deactivateUser($this->input->post('id'),$this->input->post('val'));
		$this->session->set_flashdata("success","You have successfully updated the user.");
		echo "success";
	}

	public function getCategory(){

		$r = $this->system_model->getCategory($this->input->post('id'));
		$this->output
	    ->set_content_type('application/json')
	    ->set_output(json_encode($r));
	}

	public function getSubCategory(){

		$r = $this->system_model->getSubCategory($this->input->post('id'));
		$this->output
	    ->set_content_type('application/json')
	    ->set_output(json_encode($r));
	}


	public function updateCategory(){
		$this->system_model->updateCategory($_POST);
		$this->session->set_flashdata("success","You have successfully updated the category.");
		echo "success";
		redirect(base()."/systemadmin/categories");
	}

	public function updateSubCategory(){
		$this->system_model->updateSubCategory($_POST);
		$this->session->set_flashdata("success","You have successfully updated the category.");
		echo "success";
		redirect(base()."/systemadmin/categories");
	}




}
